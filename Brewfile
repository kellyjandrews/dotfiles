# Taps
tap 'caskroom/cask'
tap 'caskroom/fonts'
tap 'caskroom/versions'
tap 'homebrew/bundle'
tap 'homebrew/services'

# Install Fish
brew 'fish'

# Install GNU core utilities (those that come with macOS are outdated)
brew 'coreutils'

# Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
brew 'findutils'

# Install Bash 4
brew 'bash'

# Install more recent versions of some macOS tools
brew 'grep'
brew 'openssh'
brew 'screen'

# Install Binaries
brew 'git'
brew 'mackup'
brew 'mas'
brew 'node'
brew 'trash'
brew 'wget'

# Development
brew 'mongodb'
brew 'docker-compose'
brew 'go'
brew 'yarn'
brew 'watchman'
brew 'heroku'

# Apps
cask '1password'
cask 'appcleaner'
cask 'atom'
cask 'balsamiq-mockups'
cask 'caffeine'
cask 'cheatsheet'
cask 'cloud'
cask 'dash'
cask 'deezer'
cask 'docker'
cask 'dropbox'
cask 'firefox'
cask 'google-chrome'
cask 'google-drive-file-stream'
cask 'iterm2'
cask 'postman'
cask 'skype'
cask 'slack'
cask 'transmit'
cask 'vagrant'
cask 'virtualbox'
cask 'virtualbox-extension-pack'

# Quicklook
cask 'qlcolorcode'
cask 'qlmarkdown'
cask 'quicklook-json'
cask 'quicklook-csv'
cask 'qlstephen'

# Install Mac App Store apps
mas 'DaisyDisk', id: 411643860
mas 'Fontcase', id: 403095673
mas 'Boom 2', id: 948176063
