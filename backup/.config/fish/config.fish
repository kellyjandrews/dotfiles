set -g fish_user_paths '~/.local/bin', '/usr/local/sbin' $fish_user_paths
set -g fish_user_paths '/usr/local/opt/icu4c/sbin' $fish_user_paths
set fish_function_path $fish_function_path /usr/local/share/fish/functions/

# FISH
alias rs "source ~/.config/fish/config.fish"

# Mac
alias of "open . -a finder"
# alias atom "atom $argv --enable-gpu-rasterization"
# GIT
alias clone='git clone'
alias co='git checkout'
alias gs='git status'
alias ga='git add'
alias gl='git log'
alias gc='git commit'
alias gd='git diff'
alias stash='git stash'
alias gf='git fetch'
alias gpick='git cherry-pick'
alias gr='git reset'
alias push='git push'
alias pull='git pull'
alias glpretty='git log --pretty="%H - %an - %cr -  %s"'

# DOCKER
alias dps='docker ps'
alias dimages='docker images'
alias dmachine='docker-machine'
alias dcompose='docker-compose'
alias removecontainers='docker rm (docker ps -a -q)'
alias removeimages='docker rmi (docker images -q)'
alias removevolumes='docker volume rm (docker volume ls -q)'
alias dockercleanup='removeimages -f; removecontainers; removevolumes'
alias up='docker-compose up'
alias down='docker-compose down'
alias dc='docker-compose run'

# NPM
alias ng 'npm list -g --depth=0 2>/dev/null'
alias nl 'npm list --depth=0 2>/dev/null'

test -e {$HOME}/.iterm2_shell_integration.fish ; and source {$HOME}/.iterm2_shell_integration.fish
# rvm default
