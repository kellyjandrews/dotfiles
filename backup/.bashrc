export NVM_DIR="/Users/kellyandrews/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# Basic Aliases
alias rs='source ~/.bashrc'
alias atom='open -a Atom'
alias apps='cd ~/Apps'


#file specific
alias hosts='atom /etc/hosts'
alias bashedit='atom ~/.bashrc'

## Colorize the ls output ##
alias ls='ls -Gp'
alias ll='ls -laGp'
alias l.='ls -dGp .*'

## a quick way to get out of current directory ##
alias cd..='cd ..'
alias cd...='cd ../../../'
alias cd....='cd ../../../../'
alias cd.....='cd ../../../../'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Git Aliases
alias clone='git clone'
alias co='git checkout'
alias gs='git status'
alias ga='git add'
alias gl='git log'
alias gc='git commit'
alias push='git push'
alias diff='git diff'
alias stash='git stash'
alias pull='git pull'
alias fetch='git fetch'

export PATH=/usr/local/bin:$PATH


BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

function parse_git_branch {
   git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\[\033[0;33m\]\u#> \[\033[0m\]\[\033[0;34m\]\W\[\033[0m\] \[\033[0;32m\]$(parse_git_branch)\[\033[0m\]\$ "
export CLICOLOR=1
export TERM=xterm-256color

alias odyssey='cd ~/Apps/olympia/odyssey/ && atom ~/Apps/olympia/odyssey/'

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
