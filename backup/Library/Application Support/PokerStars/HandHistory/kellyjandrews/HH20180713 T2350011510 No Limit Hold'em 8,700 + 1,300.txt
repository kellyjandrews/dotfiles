﻿PokerStars Hand #188687383926: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level III (25/50) - 2018/07/13 16:16:30 ET
Table '2350011510 7' 9-max Seat #5 is the button
Seat 2: MarMor2010 (1455 in chips) 
Seat 3: terriwanna (2525 in chips) 
Seat 4: kellyjandrews (1500 in chips) 
Seat 5: #1gersfan (1415 in chips) 
Seat 6: samotsirbag2 (1895 in chips) 
Seat 7: adrianos777 (1035 in chips) 
Seat 8: SoTzuMe (1080 in chips) 
Seat 9: jlbolino (1290 in chips) 
samotsirbag2: posts small blind 25
adrianos777: posts big blind 50
*** HOLE CARDS ***
Dealt to kellyjandrews [Js 7c]
SoTzuMe: folds 
jlbolino: raises 100 to 150
MarMor2010: folds 
terriwanna: folds 
kellyjandrews: folds 
#1gersfan: folds 
samotsirbag2: calls 125
adrianos777: folds 
*** FLOP *** [9c 5d 2d]
samotsirbag2: checks 
jlbolino: bets 200
samotsirbag2: calls 200
*** TURN *** [9c 5d 2d] [Jc]
samotsirbag2: checks 
jlbolino: checks 
*** RIVER *** [9c 5d 2d Jc] [2c]
samotsirbag2: checks 
jlbolino: checks 
*** SHOW DOWN ***
samotsirbag2: shows [3h Ad] (a pair of Deuces)
jlbolino: mucks hand 
samotsirbag2 collected 750 from pot
*** SUMMARY ***
Total pot 750 | Rake 0 
Board [9c 5d 2d Jc 2c]
Seat 2: MarMor2010 folded before Flop (didn't bet)
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan (button) folded before Flop (didn't bet)
Seat 6: samotsirbag2 (small blind) showed [3h Ad] and won (750) with a pair of Deuces
Seat 7: adrianos777 (big blind) folded before Flop
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino mucked [Ks Td]



PokerStars Hand #188687416941: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level III (25/50) - 2018/07/13 16:17:17 ET
Table '2350011510 7' 9-max Seat #6 is the button
Seat 1: el pipi sb (1500 in chips) 
Seat 2: MarMor2010 (1455 in chips) 
Seat 3: terriwanna (2525 in chips) 
Seat 4: kellyjandrews (1500 in chips) 
Seat 5: #1gersfan (1415 in chips) 
Seat 6: samotsirbag2 (2295 in chips) 
Seat 7: adrianos777 (985 in chips) 
Seat 8: SoTzuMe (1080 in chips) 
Seat 9: jlbolino (940 in chips) 
adrianos777: posts small blind 25
SoTzuMe: posts big blind 50
*** HOLE CARDS ***
Dealt to kellyjandrews [Ks Ts]
jlbolino: folds 
el pipi sb: calls 50
MarMor2010: raises 50 to 100
terriwanna: folds 
kellyjandrews: calls 100
#1gersfan: folds 
samotsirbag2: calls 100
adrianos777: calls 75
SoTzuMe: calls 50
el pipi sb: calls 50
*** FLOP *** [Ad 3h 6d]
adrianos777: checks 
SoTzuMe: bets 300
el pipi sb: folds 
MarMor2010: folds 
kellyjandrews: folds 
samotsirbag2: folds 
adrianos777: folds 
Uncalled bet (300) returned to SoTzuMe
SoTzuMe collected 600 from pot
SoTzuMe: doesn't show hand 
*** SUMMARY ***
Total pot 600 | Rake 0 
Board [Ad 3h 6d]
Seat 1: el pipi sb folded on the Flop
Seat 2: MarMor2010 folded on the Flop
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews folded on the Flop
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 (button) folded on the Flop
Seat 7: adrianos777 (small blind) folded on the Flop
Seat 8: SoTzuMe (big blind) collected (600)
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687451413: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level III (25/50) - 2018/07/13 16:18:07 ET
Table '2350011510 7' 9-max Seat #7 is the button
Seat 1: el pipi sb (1400 in chips) 
Seat 2: MarMor2010 (1355 in chips) 
Seat 3: terriwanna (2525 in chips) 
Seat 4: kellyjandrews (1400 in chips) 
Seat 5: #1gersfan (1415 in chips) 
Seat 6: samotsirbag2 (2195 in chips) 
Seat 7: adrianos777 (885 in chips) 
Seat 8: SoTzuMe (1580 in chips) 
Seat 9: jlbolino (940 in chips) 
SoTzuMe: posts small blind 25
jlbolino: posts big blind 50
*** HOLE CARDS ***
Dealt to kellyjandrews [Ac 6s]
el pipi sb: folds 
MarMor2010: calls 50
terriwanna: calls 50
kellyjandrews: folds 
#1gersfan: calls 50
samotsirbag2: raises 150 to 200
adrianos777: folds 
SoTzuMe: folds 
jlbolino: folds 
MarMor2010: folds 
terriwanna: calls 150
#1gersfan: folds 
*** FLOP *** [9s 2d Ah]
terriwanna: checks 
samotsirbag2: checks 
*** TURN *** [9s 2d Ah] [4s]
terriwanna: bets 50
samotsirbag2: calls 50
*** RIVER *** [9s 2d Ah 4s] [7c]
terriwanna: bets 50
samotsirbag2: calls 50
*** SHOW DOWN ***
terriwanna: shows [9h Td] (a pair of Nines)
samotsirbag2: mucks hand 
terriwanna collected 775 from pot
*** SUMMARY ***
Total pot 775 | Rake 0 
Board [9s 2d Ah 4s 7c]
Seat 1: el pipi sb folded before Flop (didn't bet)
Seat 2: MarMor2010 folded before Flop
Seat 3: terriwanna showed [9h Td] and won (775) with a pair of Nines
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan folded before Flop
Seat 6: samotsirbag2 mucked [Tc Kh]
Seat 7: adrianos777 (button) folded before Flop (didn't bet)
Seat 8: SoTzuMe (small blind) folded before Flop
Seat 9: jlbolino (big blind) folded before Flop



PokerStars Hand #188687482978: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level III (25/50) - 2018/07/13 16:18:52 ET
Table '2350011510 7' 9-max Seat #8 is the button
Seat 1: el pipi sb (1400 in chips) 
Seat 2: MarMor2010 (1305 in chips) 
Seat 3: terriwanna (3000 in chips) 
Seat 4: kellyjandrews (1400 in chips) 
Seat 5: #1gersfan (1365 in chips) 
Seat 6: samotsirbag2 (1895 in chips) 
Seat 7: adrianos777 (885 in chips) 
Seat 8: SoTzuMe (1555 in chips) 
Seat 9: jlbolino (890 in chips) 
jlbolino: posts small blind 25
el pipi sb: posts big blind 50
*** HOLE CARDS ***
Dealt to kellyjandrews [6c 7c]
MarMor2010: folds 
terriwanna: folds 
kellyjandrews: calls 50
#1gersfan: folds 
samotsirbag2: folds 
adrianos777: folds 
SoTzuMe: calls 50
jlbolino: raises 100 to 150
el pipi sb has timed out
el pipi sb: folds 
kellyjandrews: calls 100
SoTzuMe: folds 
*** FLOP *** [9h 2c 7h]
jlbolino: bets 250
kellyjandrews: raises 250 to 500
jlbolino: raises 240 to 740 and is all-in
kellyjandrews: calls 240
*** TURN *** [9h 2c 7h] [9c]
*** RIVER *** [9h 2c 7h 9c] [Ah]
*** SHOW DOWN ***
jlbolino: shows [As Ac] (a full house, Aces full of Nines)
kellyjandrews: shows [6c 7c] (two pair, Nines and Sevens)
jlbolino collected 1880 from pot
*** SUMMARY ***
Total pot 1880 | Rake 0 
Board [9h 2c 7h 9c Ah]
Seat 1: el pipi sb (big blind) folded before Flop
Seat 2: MarMor2010 folded before Flop (didn't bet)
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews showed [6c 7c] and lost with two pair, Nines and Sevens
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 folded before Flop (didn't bet)
Seat 7: adrianos777 folded before Flop (didn't bet)
Seat 8: SoTzuMe (button) folded before Flop
Seat 9: jlbolino (small blind) showed [As Ac] and won (1880) with a full house, Aces full of Nines



PokerStars Hand #188687529536: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level III (25/50) - 2018/07/13 16:20:00 ET
Table '2350011510 7' 9-max Seat #9 is the button
Seat 1: el pipi sb (1350 in chips) is sitting out
Seat 3: terriwanna (3000 in chips) 
Seat 4: kellyjandrews (510 in chips) 
Seat 5: #1gersfan (1365 in chips) 
Seat 6: samotsirbag2 (1895 in chips) 
Seat 7: adrianos777 (885 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1880 in chips) 
el pipi sb: posts small blind 25
terriwanna: posts big blind 50
*** HOLE CARDS ***
Dealt to kellyjandrews [4h 5c]
kellyjandrews: folds 
#1gersfan: folds 
SoTzuMe said, "nh"
samotsirbag2: calls 50
adrianos777: raises 150 to 200
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: folds 
terriwanna: calls 150
samotsirbag2: calls 150
*** FLOP *** [Ad Qd 7s]
terriwanna: bets 150
samotsirbag2: calls 150
adrianos777: raises 535 to 685 and is all-in
terriwanna: calls 535
samotsirbag2: calls 535
*** TURN *** [Ad Qd 7s] [6d]
terriwanna: bets 300
samotsirbag2: raises 300 to 600
terriwanna: calls 300
*** RIVER *** [Ad Qd 7s 6d] [2h]
terriwanna: checks 
samotsirbag2: bets 410 and is all-in
terriwanna: calls 410
*** SHOW DOWN ***
samotsirbag2: shows [6c Qs] (two pair, Queens and Sixes)
terriwanna: shows [Ks As] (a pair of Aces)
samotsirbag2 collected 2020 from side pot
adrianos777: shows [9h Ac] (a pair of Aces)
samotsirbag2 collected 2680 from main pot
adrianos777 finished the tournament
*** SUMMARY ***
Total pot 4700 Main pot 2680. Side pot 2020. | Rake 0 
Board [Ad Qd 7s 6d 2h]
Seat 1: el pipi sb (small blind) folded before Flop
Seat 3: terriwanna (big blind) showed [Ks As] and lost with a pair of Aces
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 showed [6c Qs] and won (4700) with two pair, Queens and Sixes
Seat 7: adrianos777 showed [9h Ac] and lost with a pair of Aces
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino (button) folded before Flop (didn't bet)



PokerStars Hand #188687580029: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:21:12 ET
Table '2350011510 7' 9-max Seat #1 is the button
Seat 1: el pipi sb (1325 in chips) 
Seat 3: terriwanna (1105 in chips) 
Seat 4: kellyjandrews (510 in chips) 
Seat 5: #1gersfan (1365 in chips) 
Seat 6: samotsirbag2 (4700 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1880 in chips) 
terriwanna: posts small blind 50
kellyjandrews: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [3s Tc]
#1gersfan: folds 
samotsirbag2: folds 
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: raises 200 to 300
terriwanna: calls 250
kellyjandrews: folds 
*** FLOP *** [5h 5s As]
terriwanna: checks 
el pipi sb: bets 350
terriwanna: folds 
Uncalled bet (350) returned to el pipi sb
el pipi sb collected 700 from pot
el pipi sb: doesn't show hand 
*** SUMMARY ***
Total pot 700 | Rake 0 
Board [5h 5s As]
Seat 1: el pipi sb (button) collected (700)
Seat 3: terriwanna (small blind) folded on the Flop
Seat 4: kellyjandrews (big blind) folded before Flop
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 folded before Flop (didn't bet)
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687604222: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:21:46 ET
Table '2350011510 7' 9-max Seat #3 is the button
Seat 1: el pipi sb (1725 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (805 in chips) 
Seat 4: kellyjandrews (410 in chips) 
Seat 5: #1gersfan (1365 in chips) 
Seat 6: samotsirbag2 (4700 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1880 in chips) 
kellyjandrews: posts small blind 50
#1gersfan: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [Ad 4c]
samotsirbag2: folds 
SoTzuMe: folds 
jlbolino: raises 200 to 300
el pipi sb: folds 
trapwa: folds 
terriwanna: folds 
kellyjandrews: raises 110 to 410 and is all-in
#1gersfan: folds 
jlbolino: calls 110
*** FLOP *** [9h 6d 7h]
*** TURN *** [9h 6d 7h] [Jd]
*** RIVER *** [9h 6d 7h Jd] [6h]
*** SHOW DOWN ***
kellyjandrews: shows [Ad 4c] (a pair of Sixes)
jlbolino: shows [Kc Td] (a pair of Sixes - lower kicker)
kellyjandrews collected 920 from pot
*** SUMMARY ***
Total pot 920 | Rake 0 
Board [9h 6d 7h Jd 6h]
Seat 1: el pipi sb folded before Flop (didn't bet)
Seat 2: trapwa folded before Flop (didn't bet)
Seat 3: terriwanna (button) folded before Flop (didn't bet)
Seat 4: kellyjandrews (small blind) showed [Ad 4c] and won (920) with a pair of Sixes
Seat 5: #1gersfan (big blind) folded before Flop
Seat 6: samotsirbag2 folded before Flop (didn't bet)
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino showed [Kc Td] and lost with a pair of Sixes



PokerStars Hand #188687625412: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:22:17 ET
Table '2350011510 7' 9-max Seat #4 is the button
Seat 1: el pipi sb (1725 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (805 in chips) 
Seat 4: kellyjandrews (920 in chips) 
Seat 5: #1gersfan (1265 in chips) 
Seat 6: samotsirbag2 (4700 in chips) 
Seat 7: zettelsraum (1500 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1470 in chips) 
#1gersfan: posts small blind 50
samotsirbag2: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [6c Ad]
zettelsraum: calls 100
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: folds 
trapwa: folds 
terriwanna: calls 100
kellyjandrews: raises 820 to 920 and is all-in
#1gersfan: folds 
samotsirbag2: folds 
zettelsraum: folds 
terriwanna: folds 
Uncalled bet (820) returned to kellyjandrews
kellyjandrews collected 450 from pot
kellyjandrews: doesn't show hand 
*** SUMMARY ***
Total pot 450 | Rake 0 
Seat 1: el pipi sb folded before Flop (didn't bet)
Seat 2: trapwa folded before Flop (didn't bet)
Seat 3: terriwanna folded before Flop
Seat 4: kellyjandrews (button) collected (450)
Seat 5: #1gersfan (small blind) folded before Flop
Seat 6: samotsirbag2 (big blind) folded before Flop
Seat 7: zettelsraum folded before Flop
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687647800: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:22:49 ET
Table '2350011510 7' 9-max Seat #5 is the button
Seat 1: el pipi sb (1725 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (705 in chips) 
Seat 4: kellyjandrews (1270 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (4600 in chips) 
Seat 7: zettelsraum (1400 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1470 in chips) 
samotsirbag2: posts small blind 50
zettelsraum: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [Qs 4c]
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: raises 200 to 300
trapwa: folds 
terriwanna: folds 
kellyjandrews: folds 
#1gersfan: folds 
samotsirbag2: calls 250
zettelsraum: calls 200
*** FLOP *** [7s Kd Ac]
samotsirbag2: checks 
zettelsraum: bets 1100 and is all-in
el pipi sb: folds 
samotsirbag2: calls 1100
*** TURN *** [7s Kd Ac] [9s]
*** RIVER *** [7s Kd Ac 9s] [9d]
*** SHOW DOWN ***
samotsirbag2: shows [Jc Qh] (a pair of Nines)
zettelsraum: shows [Ks 3h] (two pair, Kings and Nines)
zettelsraum collected 3100 from pot
*** SUMMARY ***
Total pot 3100 | Rake 0 
Board [7s Kd Ac 9s 9d]
Seat 1: el pipi sb folded on the Flop
Seat 2: trapwa folded before Flop (didn't bet)
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan (button) folded before Flop (didn't bet)
Seat 6: samotsirbag2 (small blind) showed [Jc Qh] and lost with a pair of Nines
Seat 7: zettelsraum (big blind) showed [Ks 3h] and won (3100) with two pair, Kings and Nines
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687673191: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:23:26 ET
Table '2350011510 7' 9-max Seat #6 is the button
Seat 1: el pipi sb (1425 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (705 in chips) 
Seat 4: kellyjandrews (1270 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (3200 in chips) 
Seat 7: zettelsraum (3100 in chips) 
Seat 8: SoTzuMe (1505 in chips) 
Seat 9: jlbolino (1470 in chips) 
zettelsraum: posts small blind 50
SoTzuMe: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [7d Ks]
jlbolino: folds 
el pipi sb: folds 
trapwa: folds 
terriwanna: calls 100
kellyjandrews: folds 
#1gersfan: folds 
samotsirbag2: calls 100
zettelsraum: calls 50
SoTzuMe: checks 
*** FLOP *** [Td 5s Th]
zettelsraum: checks 
SoTzuMe: checks 
terriwanna: checks 
samotsirbag2: checks 
*** TURN *** [Td 5s Th] [7s]
zettelsraum: checks 
SoTzuMe: bets 100
terriwanna: calls 100
samotsirbag2: folds 
zettelsraum: calls 100
*** RIVER *** [Td 5s Th 7s] [Ts]
zettelsraum: checks 
SoTzuMe: bets 100
terriwanna: folds 
zettelsraum: calls 100
*** SHOW DOWN ***
SoTzuMe: shows [Ac 7h] (a full house, Tens full of Sevens)
zettelsraum: mucks hand 
SoTzuMe collected 900 from pot
*** SUMMARY ***
Total pot 900 | Rake 0 
Board [Td 5s Th 7s Ts]
Seat 1: el pipi sb folded before Flop (didn't bet)
Seat 2: trapwa folded before Flop (didn't bet)
Seat 3: terriwanna folded on the River
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 (button) folded on the Turn
Seat 7: zettelsraum (small blind) mucked [5c Qc]
Seat 8: SoTzuMe (big blind) showed [Ac 7h] and won (900) with a full house, Tens full of Sevens
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687716619: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level IV (50/100) - 2018/07/13 16:24:28 ET
Table '2350011510 7' 9-max Seat #7 is the button
Seat 1: el pipi sb (1425 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (505 in chips) 
Seat 4: kellyjandrews (1270 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (3100 in chips) 
Seat 7: zettelsraum (2800 in chips) 
Seat 8: SoTzuMe (2105 in chips) 
Seat 9: jlbolino (1470 in chips) 
SoTzuMe: posts small blind 50
jlbolino: posts big blind 100
*** HOLE CARDS ***
Dealt to kellyjandrews [8h Qd]
el pipi sb: folds 
trapwa: folds 
terriwanna: calls 100
kellyjandrews: folds 
#1gersfan: folds 
samotsirbag2: calls 100
zettelsraum: calls 100
SoTzuMe: calls 50
jlbolino: checks 
*** FLOP *** [3h Th Jd]
SoTzuMe: checks 
jlbolino: checks 
terriwanna: checks 
samotsirbag2: checks 
zettelsraum: bets 2700 and is all-in
SoTzuMe: folds 
jlbolino: folds 
terriwanna: folds 
samotsirbag2: folds 
Uncalled bet (2700) returned to zettelsraum
zettelsraum collected 500 from pot
zettelsraum: doesn't show hand 
*** SUMMARY ***
Total pot 500 | Rake 0 
Board [3h Th Jd]
Seat 1: el pipi sb folded before Flop (didn't bet)
Seat 2: trapwa folded before Flop (didn't bet)
Seat 3: terriwanna folded on the Flop
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 folded on the Flop
Seat 7: zettelsraum (button) collected (500)
Seat 8: SoTzuMe (small blind) folded on the Flop
Seat 9: jlbolino (big blind) folded on the Flop



PokerStars Hand #188687748746: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level V (75/150) - 2018/07/13 16:25:15 ET
Table '2350011510 7' 9-max Seat #8 is the button
Seat 1: el pipi sb (1425 in chips) 
Seat 2: trapwa (1500 in chips) 
Seat 3: terriwanna (405 in chips) 
Seat 4: kellyjandrews (1270 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (3000 in chips) 
Seat 7: zettelsraum (3200 in chips) 
Seat 8: SoTzuMe (2005 in chips) 
Seat 9: jlbolino (1370 in chips) 
jlbolino: posts small blind 75
el pipi sb: posts big blind 150
*** HOLE CARDS ***
Dealt to kellyjandrews [Ah Qc]
trapwa: calls 150
terriwanna: folds 
kellyjandrews: raises 150 to 300
#1gersfan: folds 
samotsirbag2: folds 
zettelsraum: folds 
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: calls 150
trapwa: calls 150
*** FLOP *** [3h Td 9s]
el pipi sb: checks 
trapwa: bets 150
kellyjandrews: calls 150
el pipi sb: folds 
*** TURN *** [3h Td 9s] [3d]
trapwa: bets 150
kellyjandrews: calls 150
*** RIVER *** [3h Td 9s 3d] [2h]
trapwa: bets 150
kellyjandrews: calls 150
*** SHOW DOWN ***
trapwa: shows [9h 8h] (two pair, Nines and Threes)
kellyjandrews: mucks hand 
trapwa collected 1875 from pot
*** SUMMARY ***
Total pot 1875 | Rake 0 
Board [3h Td 9s 3d 2h]
Seat 1: el pipi sb (big blind) folded on the Flop
Seat 2: trapwa showed [9h 8h] and won (1875) with two pair, Nines and Threes
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews mucked [Ah Qc]
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 folded before Flop (didn't bet)
Seat 7: zettelsraum folded before Flop (didn't bet)
Seat 8: SoTzuMe (button) folded before Flop (didn't bet)
Seat 9: jlbolino (small blind) folded before Flop



PokerStars Hand #188687783518: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level V (75/150) - 2018/07/13 16:26:05 ET
Table '2350011510 7' 9-max Seat #9 is the button
Seat 1: el pipi sb (1125 in chips) 
Seat 2: trapwa (2625 in chips) 
Seat 3: terriwanna (405 in chips) 
Seat 4: kellyjandrews (520 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (3000 in chips) 
Seat 7: zettelsraum (3200 in chips) 
Seat 8: SoTzuMe (2005 in chips) 
Seat 9: jlbolino (1295 in chips) 
el pipi sb: posts small blind 75
trapwa: posts big blind 150
*** HOLE CARDS ***
Dealt to kellyjandrews [Qc Qd]
terriwanna: folds 
kellyjandrews: raises 370 to 520 and is all-in
#1gersfan: folds 
samotsirbag2: folds 
zettelsraum: calls 520
SoTzuMe: folds 
jlbolino: folds 
el pipi sb: folds 
trapwa: folds 
*** FLOP *** [9c 4h Ks]
*** TURN *** [9c 4h Ks] [Jc]
*** RIVER *** [9c 4h Ks Jc] [4d]
*** SHOW DOWN ***
kellyjandrews: shows [Qc Qd] (two pair, Queens and Fours)
zettelsraum: shows [Qs Jh] (two pair, Jacks and Fours)
kellyjandrews collected 1265 from pot
*** SUMMARY ***
Total pot 1265 | Rake 0 
Board [9c 4h Ks Jc 4d]
Seat 1: el pipi sb (small blind) folded before Flop
Seat 2: trapwa (big blind) folded before Flop
Seat 3: terriwanna folded before Flop (didn't bet)
Seat 4: kellyjandrews showed [Qc Qd] and won (1265) with two pair, Queens and Fours
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 folded before Flop (didn't bet)
Seat 7: zettelsraum showed [Qs Jh] and lost with two pair, Jacks and Fours
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino (button) folded before Flop (didn't bet)



PokerStars Hand #188687814940: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level V (75/150) - 2018/07/13 16:26:51 ET
Table '2350011510 7' 9-max Seat #1 is the button
Seat 1: el pipi sb (1050 in chips) 
Seat 2: trapwa (2475 in chips) 
Seat 3: terriwanna (405 in chips) 
Seat 4: kellyjandrews (1265 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (3000 in chips) 
Seat 7: zettelsraum (2680 in chips) 
Seat 8: SoTzuMe (2005 in chips) 
Seat 9: jlbolino (1295 in chips) 
trapwa: posts small blind 75
terriwanna: posts big blind 150
*** HOLE CARDS ***
Dealt to kellyjandrews [5d 4c]
kellyjandrews: folds 
#1gersfan: folds 
samotsirbag2: calls 150
zettelsraum: folds 
SoTzuMe: folds 
jlbolino: raises 300 to 450
el pipi sb: folds 
trapwa: folds 
terriwanna: folds 
samotsirbag2: calls 300
*** FLOP *** [3d Qh As]
samotsirbag2: checks 
jlbolino: bets 845 and is all-in
samotsirbag2: calls 845
*** TURN *** [3d Qh As] [Td]
*** RIVER *** [3d Qh As Td] [9c]
*** SHOW DOWN ***
samotsirbag2: shows [6d Kd] (high card Ace)
jlbolino: shows [Jd Ac] (a pair of Aces)
jlbolino collected 2815 from pot
*** SUMMARY ***
Total pot 2815 | Rake 0 
Board [3d Qh As Td 9c]
Seat 1: el pipi sb (button) folded before Flop (didn't bet)
Seat 2: trapwa (small blind) folded before Flop
Seat 3: terriwanna (big blind) folded before Flop
Seat 4: kellyjandrews folded before Flop (didn't bet)
Seat 5: #1gersfan folded before Flop (didn't bet)
Seat 6: samotsirbag2 showed [6d Kd] and lost with high card Ace
Seat 7: zettelsraum folded before Flop (didn't bet)
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino showed [Jd Ac] and won (2815) with a pair of Aces



PokerStars Hand #188687840598: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level V (75/150) - 2018/07/13 16:27:28 ET
Table '2350011510 7' 9-max Seat #2 is the button
Seat 2: trapwa (2400 in chips) 
Seat 3: terriwanna (255 in chips) 
Seat 4: kellyjandrews (1265 in chips) 
Seat 5: #1gersfan (1215 in chips) 
Seat 6: samotsirbag2 (1705 in chips) 
Seat 7: zettelsraum (2680 in chips) 
Seat 8: SoTzuMe (2005 in chips) 
Seat 9: jlbolino (2815 in chips) 
terriwanna: posts small blind 75
kellyjandrews: posts big blind 150
*** HOLE CARDS ***
Dealt to kellyjandrews [7h 5h]
#1gersfan: calls 150
samotsirbag2: raises 525 to 675
zettelsraum: folds 
SoTzuMe: folds 
jlbolino: folds 
trapwa: folds 
terriwanna: calls 180 and is all-in
kellyjandrews: folds 
#1gersfan: folds 
Uncalled bet (420) returned to samotsirbag2
*** FLOP *** [Ts 3s 9h]
*** TURN *** [Ts 3s 9h] [8s]
*** RIVER *** [Ts 3s 9h 8s] [6d]
*** SHOW DOWN ***
terriwanna: shows [2s Qc] (high card Queen)
samotsirbag2: shows [6c 4s] (a pair of Sixes)
samotsirbag2 collected 810 from pot
terriwanna finished the tournament
*** SUMMARY ***
Total pot 810 | Rake 0 
Board [Ts 3s 9h 8s 6d]
Seat 2: trapwa (button) folded before Flop (didn't bet)
Seat 3: terriwanna (small blind) showed [2s Qc] and lost with high card Queen
Seat 4: kellyjandrews (big blind) folded before Flop
Seat 5: #1gersfan folded before Flop
Seat 6: samotsirbag2 showed [6c 4s] and won (810) with a pair of Sixes
Seat 7: zettelsraum folded before Flop (didn't bet)
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino folded before Flop (didn't bet)



PokerStars Hand #188687860693: Tournament #2350011510, 8700+1300 Hold'em No Limit - Level V (75/150) - 2018/07/13 16:27:58 ET
Table '2350011510 7' 9-max Seat #4 is the button
Seat 1: agumanga (1500 in chips) 
Seat 2: trapwa (2400 in chips) 
Seat 4: kellyjandrews (1115 in chips) 
Seat 5: #1gersfan (1065 in chips) 
Seat 6: samotsirbag2 (2260 in chips) 
Seat 7: zettelsraum (2680 in chips) 
Seat 8: SoTzuMe (2005 in chips) 
Seat 9: jlbolino (2815 in chips) 
#1gersfan: posts small blind 75
samotsirbag2: posts big blind 150
*** HOLE CARDS ***
Dealt to kellyjandrews [6c Kc]
zettelsraum: folds 
SoTzuMe: folds 
jlbolino: calls 150
agumanga: folds 
trapwa: folds 
kellyjandrews: calls 150
#1gersfan: folds 
samotsirbag2: checks 
*** FLOP *** [2c 4c Th]
samotsirbag2: bets 2110 and is all-in
jlbolino: folds 
kellyjandrews: calls 965 and is all-in
Uncalled bet (1145) returned to samotsirbag2
*** TURN *** [2c 4c Th] [Ah]
*** RIVER *** [2c 4c Th Ah] [Td]
*** SHOW DOWN ***
samotsirbag2: shows [4h 7d] (two pair, Tens and Fours)
kellyjandrews: shows [6c Kc] (a pair of Tens)
samotsirbag2 collected 2455 from pot
kellyjandrews finished the tournament
*** SUMMARY ***
Total pot 2455 | Rake 0 
Board [2c 4c Th Ah Td]
Seat 1: agumanga folded before Flop (didn't bet)
Seat 2: trapwa folded before Flop (didn't bet)
Seat 4: kellyjandrews (button) showed [6c Kc] and lost with a pair of Tens
Seat 5: #1gersfan (small blind) folded before Flop
Seat 6: samotsirbag2 (big blind) showed [4h 7d] and won (2455) with two pair, Tens and Fours
Seat 7: zettelsraum folded before Flop (didn't bet)
Seat 8: SoTzuMe folded before Flop (didn't bet)
Seat 9: jlbolino folded on the Flop



