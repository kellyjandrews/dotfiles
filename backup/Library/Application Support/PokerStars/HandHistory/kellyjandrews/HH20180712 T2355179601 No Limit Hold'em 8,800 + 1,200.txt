﻿PokerStars Hand #188644085109: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:24:45 ET
Table '2355179601 1' 3-max Seat #1 is the button
Seat 1: Galaxyuk27 (500 in chips) 
Seat 2: kellyjandrews (500 in chips) 
Seat 3: WestKenW14 (500 in chips) 
kellyjandrews: posts small blind 10
WestKenW14: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [2c Ks]
Galaxyuk27: raises 50 to 70
kellyjandrews: folds 
WestKenW14: calls 50
*** FLOP *** [7s 3s Ad]
WestKenW14: checks 
Galaxyuk27: bets 140
WestKenW14: folds 
Uncalled bet (140) returned to Galaxyuk27
Galaxyuk27 collected 150 from pot
*** SUMMARY ***
Total pot 150 | Rake 0 
Board [7s 3s Ad]
Seat 1: Galaxyuk27 (button) collected (150)
Seat 2: kellyjandrews (small blind) folded before Flop
Seat 3: WestKenW14 (big blind) folded on the Flop



PokerStars Hand #188644096172: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:25:04 ET
Table '2355179601 1' 3-max Seat #2 is the button
Seat 1: Galaxyuk27 (580 in chips) 
Seat 2: kellyjandrews (490 in chips) 
Seat 3: WestKenW14 (430 in chips) 
WestKenW14: posts small blind 10
Galaxyuk27: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [2h 9d]
kellyjandrews: folds 
WestKenW14: calls 10
Galaxyuk27: checks 
*** FLOP *** [3h 8c Js]
WestKenW14: checks 
Galaxyuk27: bets 80
WestKenW14: folds 
Uncalled bet (80) returned to Galaxyuk27
Galaxyuk27 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [3h 8c Js]
Seat 1: Galaxyuk27 (big blind) collected (40)
Seat 2: kellyjandrews (button) folded before Flop (didn't bet)
Seat 3: WestKenW14 (small blind) folded on the Flop



PokerStars Hand #188644111171: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:25:28 ET
Table '2355179601 1' 3-max Seat #3 is the button
Seat 1: Galaxyuk27 (600 in chips) 
Seat 2: kellyjandrews (490 in chips) 
Seat 3: WestKenW14 (410 in chips) 
Galaxyuk27: posts small blind 10
kellyjandrews: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [9c Js]
WestKenW14: folds 
Galaxyuk27: calls 10
kellyjandrews: checks 
*** FLOP *** [8d 6h Qd]
Galaxyuk27: bets 60
kellyjandrews: folds 
Uncalled bet (60) returned to Galaxyuk27
Galaxyuk27 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [8d 6h Qd]
Seat 1: Galaxyuk27 (small blind) collected (40)
Seat 2: kellyjandrews (big blind) folded on the Flop
Seat 3: WestKenW14 (button) folded before Flop (didn't bet)



PokerStars Hand #188644124953: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:25:51 ET
Table '2355179601 1' 3-max Seat #1 is the button
Seat 1: Galaxyuk27 (620 in chips) 
Seat 2: kellyjandrews (470 in chips) 
Seat 3: WestKenW14 (410 in chips) 
kellyjandrews: posts small blind 10
WestKenW14: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [7c 8s]
Galaxyuk27: folds 
kellyjandrews: folds 
Uncalled bet (10) returned to WestKenW14
WestKenW14 collected 20 from pot
*** SUMMARY ***
Total pot 20 | Rake 0 
Seat 1: Galaxyuk27 (button) folded before Flop (didn't bet)
Seat 2: kellyjandrews (small blind) folded before Flop
Seat 3: WestKenW14 (big blind) collected (20)



PokerStars Hand #188644130127: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:25:59 ET
Table '2355179601 1' 3-max Seat #2 is the button
Seat 1: Galaxyuk27 (620 in chips) 
Seat 2: kellyjandrews (460 in chips) 
Seat 3: WestKenW14 (420 in chips) 
WestKenW14: posts small blind 10
Galaxyuk27: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [Qh Qs]
kellyjandrews: raises 20 to 40
WestKenW14: calls 30
Galaxyuk27: calls 20
*** FLOP *** [Ah 8c Kd]
WestKenW14: checks 
Galaxyuk27: checks 
kellyjandrews: bets 120
WestKenW14: folds 
Galaxyuk27: folds 
Uncalled bet (120) returned to kellyjandrews
kellyjandrews collected 120 from pot
kellyjandrews: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [Ah 8c Kd]
Seat 1: Galaxyuk27 (big blind) folded on the Flop
Seat 2: kellyjandrews (button) collected (120)
Seat 3: WestKenW14 (small blind) folded on the Flop



PokerStars Hand #188644146087: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:26:26 ET
Table '2355179601 1' 3-max Seat #3 is the button
Seat 1: Galaxyuk27 (580 in chips) 
Seat 2: kellyjandrews (540 in chips) 
Seat 3: WestKenW14 (380 in chips) 
Galaxyuk27: posts small blind 10
kellyjandrews: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [5h 8h]
WestKenW14: calls 20
Galaxyuk27: calls 10
kellyjandrews: checks 
*** FLOP *** [5d 2c 6c]
Galaxyuk27: bets 60
kellyjandrews: calls 60
WestKenW14: raises 60 to 120
Galaxyuk27: calls 60
kellyjandrews: folds 
*** TURN *** [5d 2c 6c] [Qs]
Galaxyuk27: checks 
WestKenW14: bets 180
Galaxyuk27: calls 180
*** RIVER *** [5d 2c 6c Qs] [8s]
Galaxyuk27: bets 40
WestKenW14: raises 20 to 60 and is all-in
Galaxyuk27: calls 20
*** SHOW DOWN ***
WestKenW14: shows [4d As] (high card Ace)
Galaxyuk27: shows [9d 6h] (a pair of Sixes)
Galaxyuk27 collected 840 from pot
WestKenW14 finished the tournament in 3rd place
*** SUMMARY ***
Total pot 840 | Rake 0 
Board [5d 2c 6c Qs 8s]
Seat 1: Galaxyuk27 (small blind) showed [9d 6h] and won (840) with a pair of Sixes
Seat 2: kellyjandrews (big blind) folded on the Flop
Seat 3: WestKenW14 (button) showed [4d As] and lost with high card Ace



PokerStars Hand #188644178288: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:27:18 ET
Table '2355179601 1' 3-max Seat #2 is the button
Seat 1: Galaxyuk27 (1040 in chips) 
Seat 2: kellyjandrews (460 in chips) 
kellyjandrews: posts small blind 10
Galaxyuk27: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [3s 9s]
kellyjandrews: calls 10
Galaxyuk27: checks 
*** FLOP *** [Qc 6s 3d]
Galaxyuk27: checks 
kellyjandrews: bets 20
Galaxyuk27: folds 
Uncalled bet (20) returned to kellyjandrews
kellyjandrews collected 40 from pot
kellyjandrews: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [Qc 6s 3d]
Seat 1: Galaxyuk27 (big blind) folded on the Flop
Seat 2: kellyjandrews (button) (small blind) collected (40)



PokerStars Hand #188644188567: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:27:35 ET
Table '2355179601 1' 3-max Seat #1 is the button
Seat 1: Galaxyuk27 (1020 in chips) 
Seat 2: kellyjandrews (480 in chips) 
Galaxyuk27: posts small blind 10
kellyjandrews: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [4c 7h]
Galaxyuk27: raises 60 to 80
kellyjandrews: folds 
Uncalled bet (60) returned to Galaxyuk27
Galaxyuk27 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 1: Galaxyuk27 (button) (small blind) collected (40)
Seat 2: kellyjandrews (big blind) folded before Flop



PokerStars Hand #188644193898: Tournament #2355179601, 8800+1200 Hold'em No Limit - Level I (10/20) - 2018/07/12 13:27:44 ET
Table '2355179601 1' 3-max Seat #2 is the button
Seat 1: Galaxyuk27 (1040 in chips) 
Seat 2: kellyjandrews (460 in chips) 
kellyjandrews: posts small blind 10
Galaxyuk27: posts big blind 20
*** HOLE CARDS ***
Dealt to kellyjandrews [3h Qh]
kellyjandrews: calls 10
Galaxyuk27: raises 60 to 80
kellyjandrews: raises 380 to 460 and is all-in
Galaxyuk27: calls 380
*** FLOP *** [6d 8c 5s]
*** TURN *** [6d 8c 5s] [9c]
*** RIVER *** [6d 8c 5s 9c] [3c]
*** SHOW DOWN ***
Galaxyuk27: shows [9d Ac] (a pair of Nines)
kellyjandrews: shows [3h Qh] (a pair of Threes)
Galaxyuk27 collected 920 from pot
kellyjandrews finished the tournament in 2nd place
Galaxyuk27 wins the tournament and receives 15000.00 - congratulations!
*** SUMMARY ***
Total pot 920 | Rake 0 
Board [6d 8c 5s 9c 3c]
Seat 1: Galaxyuk27 (big blind) showed [9d Ac] and won (920) with a pair of Nines
Seat 2: kellyjandrews (button) (small blind) showed [3h Qh] and lost with a pair of Threes



