#!/usr/bin/env bash

# Thanks to Mathias Bynens! https://mths.be/macos

# Ask for the administrator password upfront
# sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macos` has finished
# while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &


##############################################
# General UI/UX                              #
##############################################

# Disable the sound effects on boot
sudo nvram SystemAudioVolume=" "
# Set sidebar icon size to medium
defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2
# Show hidden files in finder
defaults write com.apple.finder AppleShowAllFiles YES
# Disable smart dashes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
# Disable smart quotes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
# Disable the “Are you sure you want to open this application?” dialog
defaults write com.apple.LaunchServices LSQuarantine -bool false
# Remove duplicates in the “Open With” menu (also see `lscleanup` alias)
/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user

###############################################################
# Trackpad, mouse, keyboard, Bluetooth accessories, and input #
###############################################################

# Disable auto-correct
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

##############################################
# Software Updates                           #
##############################################

# Turns off software autocheck
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled -bool false

##############################################
# Finder                                     #
##############################################

# Change default screenshot location
mkdir -p ~/Documents/Screenshots
defaults write com.apple.screencapture location ~/Documents/Screenshots
# Disable window animations and Get Info animations
defaults write com.apple.finder DisableAllAnimations -bool true
# Show path bar
defaults write com.apple.finder ShowPathbar -bool true
# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"
# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
# Use list view in all Finder windows by default
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"
# Disable the warning before emptying the Trash
defaults write com.apple.finder WarnOnEmptyTrash -bool false
# Expand the following File Info panes:
# “General”, “Open with”, and “Sharing & Permissions”
defaults write com.apple.finder FXInfoPanesExpanded -dict \
    General -bool true \
    OpenWith -bool true \
    Privileges -bool true
# Auto hide menu bar
defaults write NSGlobalDomain _HIHideMenuBar -bool true

##############################################
# Dock and Dashboard                         #
##############################################

# Minimize windows into their application’s icon
defaults write com.apple.dock minimize-to-application -bool true
# Change minimize/maximize window effect
defaults write com.apple.dock mineffect -string "scale"
# Wipe all (default) app icons from the Dock
defaults write com.apple.dock persistent-apps -array
# Autohide dock
defaults write com.apple.dock autohide -bool true
# Only show active applications on dock
defaults write com.apple.dock static-only -bool true
# Hide process indicators
defaults write com.apple.dock show-process-indicators -bool false
# Set icon size
defaults write com.apple.dock tilesize -int 45
# Hide desktop icons
defaults write com.apple.finder CreateDesktop -bool false

##############################################
# Terminal & iTerm 2                         #
##############################################

# Only use UTF-8 in Terminal.app
defaults write com.apple.terminal StringEncodings -array 4

# Don’t display the annoying prompt when quitting iTerm
defaults write com.googlecode.iterm2 PromptOnQuit -bool false
defaults write com.googlecode.iterm2 PrefsCustomFolder -string "${PWD}/backup/Library/Preferences"
defaults write com.googlecode.iterm2 LoadPrefsFromCustomFolder -bool true

##############################################
# Time Machine                               #
##############################################

# Prevent Time Machine from prompting to use new hard drives as backup volume
defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

##############################################
# Kill affected applications                 #
##############################################

for app in "Finder" "Dock" "SystemUIServer"; do
    killall "${app}" &> /dev/null
done
