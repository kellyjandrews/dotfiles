# Setting up a new mac

1. Update macOS to latest
2. Install Xcode and CLI tools - `xcode-select --install` - open xcode to complete
3. Download repo as zip file, and run `cp -a downloaded/repo/folder ~/.dotfiles`
4. Add `/usr/local/bin/fish` to the end of `/etc/shells`
5. Run `./setup.sh`
6. Add Public SSH key to Github/Gitlab etc.
7. Set up git and remote in `~/.dotfiles`
 + git init
 + git remote add origin PATH/TO/REPO
 + git fetch
 + git reset origin/master
8. Restart your computer to finalize the process

Your Mac is now ready to use!
