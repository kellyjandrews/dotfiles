#!/bin/sh

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
BOLD='\033[1m'
NB='\033[21m' # No Bold
NC='\033[0m' # No Color
SSH_KEYNAME='git_id_rsa'
EMAIL_ADDRESS='kelly@kellyjandrews.com'

printf "${GREEN}Setting up your Mac...${NC}\n"
sudo -v
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

###############################################################################
# Homebrew                                                                    #
###############################################################################
# Check for Homebrew and install if we don't have it
if test ! $(which brew); then
  printf "${GREEN}Installing Homebrew...${NC}\n"
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  sudo chown -R $(whoami) /usr/local/
  brew doctor
fi

# Update Homebrew recipes
printf "${GREEN}Updating Homebrew...${NC}\n"
brew update

# Upgrade Homebrew recipes
printf "${GREEN}Upgrading Homebrew packages...${NC}\n"
brew upgrade

# Install all our dependencies with bundle (See Brewfile)
printf "${GREEN}Tapping your bundle...${NC}\n"
brew tap homebrew/bundle
printf "${GREEN}Installing your bundle...${NC}\n"
brew bundle >/dev/null

# Clean up outdated versions
printf "${GREEN}Cleaning the cellar...${NC}\n"
brew cleanup

###############################################################################
# Fish, Fundle, OMF                                                           #
###############################################################################
if [[ "${SHELL}" != "/usr/local/bin/fish" ]]; then
  # Make FISH the default shell environment
  printf "${GREEN}Making Fish the default...${NC}\n"
  chsh -s $(which fish)
fi

if [[ ! -e "${HOME}/.iterm2_shell_integration.fish" ]]; then
  printf "${GREEN}Integrating Fish with iTerm2...${NC}\n"
  curl -L https://iterm2.com/misc/fish_startup.in \
  -o ~/.iterm2_shell_integration.fish
fi

printf "${GREEN}Fish is your default shell...${NC}\n"


###############################################################################
# NPM                                                                         #
###############################################################################
# printf "${GREEN}Installing Yarn modules...${NC}\n"
# yarn global add aglio n nodemon npm eslint jshint eslint-plugin-react webpack >/dev/null
printf "${GREEN}Installing NPM modules...${NC}\n"
# validate the install or need for install/update here - this gets old to wait for
npm i -g aglio n nodemon eslint jshint eslint-plugin-react webpack yo >/dev/null


###############################################################################
# Mackup                                                                      #
###############################################################################
if [[ ! -L "~/.mackup.cfg" ]]; then
  printf "${GREEN}Creating mackup.cfg symlink...${NC}\n"
  rm -rf ~/.mackup.cfg
  ln -s ${PWD}/backup/.mackup.cfg ~/.mackup.cfg
fi

printf "${GREEN}Restoring from Mackup...${NC}\n"
mackup restore

###############################################################################
# Atom                                                                        #
###############################################################################
# Check if the Atom package folder is not symlink or doesn't exist - remove and symlink to backup
if [[ ! -L "${HOME}/.atom/packages" || ! -d "${HOME}/.atom/packages" ]] ; then
    printf "${GREEN}Setting up Atom package symlink...${NC}\n"
    rm -rf ${HOME}/.atom/packages
    ln -s ${PWD}/backup/.atom/packages ${HOME}/.atom/packages
fi

###############################################################################
# Fonts                                                                       #
###############################################################################
# Check fonts folder and install any missing, required fonts.
for font in ${PWD}/fonts/*; do
    if [[ ! -e "/Library/Fonts/$(basename "$font")" ]]; then
      printf "${GREEN}Copying $(basename "$font") to your Fonts folder...${NC}\n"
      sudo cp -a "$font"  /Library/Fonts
    fi
done

###############################################################################
# SSH Keys                                                                    #
###############################################################################
# Check if you have ssh keys created, and create if needed.
if [[ ! -e "${HOME}/.ssh/git_id_rsa" ]]; then
  printf "${RED}${BOLD}Creating SSH key...${NC}\n"
  ATTEMPTS=0
  while [[ "$ATTEMPTS" < "3" ]]; do
    printf "${BLUE}Please enter a passphrase (or leave blank to skip):${NC}"
    read -s pass
    printf "\n"
    if [[ $pass ]]; then
      printf "${BLUE}Verify passphrase:${NC}"
      read -s pass_verify
      printf "\n"
      if [[ "$pass" == "$pass_verify" ]]; then
        PASSPHRASE=$pass
        break
      else
        printf "${RED}Passphrase did not validate...${NC}\n"
        ((ATTEMPTS++))
      fi
    else
      PASSPHRASE=""
      break
    fi
  done
  #actually create the ssh
  if [[ $PASSPHRASE ]]; then
    ssh-keygen -q -b 4096 -C "${EMAIL_ADDRESS}" -t rsa -N "${PASSPHRASE}" -f ${HOME}/.ssh/${SSH_KEYNAME}
    printf "${GREEN}Add SSH Key to Keychain...${NC}\n"
    ssh-add -k ${HOME}/.ssh/${SSH_KEYNAME}
    printf "${GREEN}${BOLD}SSH Key generated...${NC}\n"
    printf "${GREEN}Use 'pbcopy < ${HOME}/.ssh/${SSH_KEYNAME}.pub' and add to remote settings...${NC}\n"
  else
    printf "${RED}${BOLD}SSH Key not generated...${NC}\n"
    printf "${RED}${BOLD}You have exceeded max attempts - run set up again...${NC}\n"
  fi
fi


###############################################################################
# macOS                                                                        #
###############################################################################
# Set macOS preferences
printf "${GREEN}Updating macOS preferences...${NC}\n"
source .macos

echo "Your mac is now setup. Some changes may require a logout/restart to take effect."
